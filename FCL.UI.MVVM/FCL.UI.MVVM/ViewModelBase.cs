﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCL.UI.MVVM
{
    public class ViewModelBase : BindableBase
    {
        virtual public void OnNavigatedTo(object data)
        {
        }
        virtual public void OnNavigatedFrom()
        {
        }
    }
}
