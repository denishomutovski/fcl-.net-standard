﻿using FCL.UI.MVVM.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FCL.UI.MVVM
{
    public class AsyncCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private bool _isExecuting;
        private readonly Func<Task> _execute;
        private readonly Func<object, Task> _executeWithParam;
        private readonly Func<bool> _canExecute;
        private readonly Func<object, bool> _canExecuteWithParam;
        private readonly IErrorHandler _errorHandler;

        public AsyncCommand(Func<Task> execute, Func<bool> canExecute = null, IErrorHandler errorHandler = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
            _errorHandler = errorHandler;
        }

        public AsyncCommand(Func<object, Task> execute, Func<object, bool> canExecute = null, IErrorHandler errorHandler = null)
        {
            _executeWithParam = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecuteWithParam = canExecute;
            _errorHandler = errorHandler;
        }

        public bool CanExecute(object parameter)
        {
            if (_isExecuting)
            {
                return false;
            }
            else if (_canExecute != null)
            {
                return _canExecute();
            }
            else if (_canExecuteWithParam != null)
            {
                return _canExecuteWithParam(parameter);
            }

            return true;
        }

        public async void Execute(object parameter)
        {
            try
            {
                await ExecuteAsync(parameter).ConfigureAwait(true);
            }
#pragma warning disable CA1031 // Не перехватывать исключения общих типов
            // Перехватываем все исключения чтобы передать их в обработчик
            catch (Exception ex)
#pragma warning restore CA1031 // Не перехватывать исключения общих типов
            {
                _errorHandler?.OnError(ex);
            }
        }

        private async Task ExecuteAsync(object parameter)
        {
            if (CanExecute(parameter))
            {
                try
                {
                    _isExecuting = true;

                    if (_execute != null)
                    {
                        await _execute().ConfigureAwait(true);
                    }
                    else if (_executeWithParam != null)
                    {
                        await _executeWithParam(parameter).ConfigureAwait(true);
                    }
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            CanExecuteChangedRaise();
        }

        public void CanExecuteChangedRaise()
        {
            Tools.EventRaise(CanExecuteChanged);
        }
    }
}
