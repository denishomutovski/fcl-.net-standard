﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCL.UI.MVVM
{
    public static class Tools
    {
        static public void EventRaise(EventHandler handler, object sender = null, EventArgs arguments = null)
        {
            handler?.Invoke(sender, arguments);
        }

        static public void EventRaise<T>(EventHandler<T> handler, object sender = null, T arguments = default(T))
        {
            handler?.Invoke(sender, arguments);
        }
    }
}
