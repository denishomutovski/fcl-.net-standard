﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCL.UI.MVVM.Services
{
    public interface ILocalizationService
    {
        string GetString(string resource);
    }
}
