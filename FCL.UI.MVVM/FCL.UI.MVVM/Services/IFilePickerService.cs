﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FCL.UI.MVVM.Services
{
	public class AttachedFileInfo
	{
		public string Name { get; set; }
		public string Path { get; set; }
#pragma warning disable CA1819 // Свойства не должны возвращать массивы
		// Предупреждение подавляется т.к. свойство является частью класса Передача данных Object (DTO)
		public byte[] Data { get; set; }
#pragma warning restore CA1819 // Свойства не должны возвращать массивы
    }

	public interface IFilePickerService
	{
		Task SaveDataToFileAsync(string fileName, byte[] data);
		Task<IEnumerable<AttachedFileInfo>> SelectAndLoadFilesDataAsync();
	}
}
