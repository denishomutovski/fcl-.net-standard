﻿using System;
using System.Threading.Tasks;

namespace FCL.UI.MVVM.Services
{
	public interface IMessageService
	{
		Task ShowInfoMessageAsync(string title, string message, string closeButtonText);
		Task<bool> ShowDialogAsync(string title, string message, string acceptButtonText, string rejectButtonText);

		Task ShowErrorMessageAsync(Exception ex);
	}
}
