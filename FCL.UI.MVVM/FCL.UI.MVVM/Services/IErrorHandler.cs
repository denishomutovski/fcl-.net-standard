﻿using System;

namespace FCL.UI.MVVM.Services
{
    public interface IErrorHandler
    {
        void SetMessageService(IMessageService messageService);

        void OnError(Exception ex, bool silent = false);
    }
}
