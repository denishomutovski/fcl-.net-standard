﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace FCL.UI.MVVM.Validation
{
    public interface IValidatableProperty : INotifyPropertyChanged
    {
        ObservableCollection<string> Errors { get; }
        bool IsValid { get; }
        bool IsDirty { get; }
        void Revert();
    }

    public class ValidatableProperty<T> : BindableBase, IValidatableProperty
    {
        public ValidatableProperty()
        {
            Errors.CollectionChanged += (s, e) => OnPropertyChanged(nameof(IsValid));
        }

        public ObservableCollection<string> Errors { get; private set; } = new ObservableCollection<string>();

        public bool IsDirty
        {
            get
            {
                return (Value == null)
                    ? Original != null
                    : !Value.Equals(Original);
            }
        }

        public bool IsValid
        {
            get { return !Errors.Any(); }
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }

        private T _original;
        public T Original
        {
            get { return _original; }
            set
            {
                _valueHasBeenSet = true;
                SetProperty(ref _original, value);
            }
        }

        private bool _valueHasBeenSet;

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                if (!_valueHasBeenSet)
                {
                    Original = value;
                }
                SetProperty(ref _value, value);
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public void Revert()
        {
            Value = Original;
        }

        public override string ToString()
        {
            return Value?.ToString() ?? string.Empty;
        }
    }
}
