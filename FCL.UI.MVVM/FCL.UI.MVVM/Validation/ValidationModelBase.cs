﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace FCL.UI.MVVM.Validation
{
    public interface IValidationModel
    {
        ObservableCollection<string> Errors { get; }
        Action<IValidationModel> Validator { get; set; }
        bool IsValid { get; }
        bool IsDirty { get; }
        bool Validate();
        void Revert();
    }

    public abstract class ValidationModelBase<T> : BindableBase, IValidationModel where T : IValidationModel 
    {
        protected ValidationModelBase()
        {
            // whenever a property is changed, validate the model
            foreach (var property in Properties().Select(x => x.GetValue(this) as IValidatableProperty))
            {
                property.PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName.Equals("Value", StringComparison.Ordinal )) Validate();
                };
            }
        }

        public ObservableCollection<string> Errors { get; } = new ObservableCollection<string>();

        private bool _isValid = true;
        public bool IsValid 
        {
            get { return _isValid; }
            set { SetProperty(ref _isValid, value); } 
        }

        private bool _isDirty;
        public bool IsDirty
        {
            get { return _isDirty; }
            set { SetProperty(ref _isDirty, value); }
        }

        private Action<IValidationModel> _validator;
        public Action<IValidationModel> Validator 
        {
            get { return _validator; }
            set { SetProperty(ref _validator, value); }
        }

        public void Revert()
        {
            foreach (var property in Properties().Select(x => x.GetValue(this) as IValidatableProperty))
            {
                property.Revert();
            }
        }

        public bool Validate()
        {
            var properties = Properties().Select(x => x.GetValue(this) as IValidatableProperty);

            foreach (var property in properties)
            {
                property.Errors.Clear();
            }
            Errors.Clear();

            Validator?.Invoke(this);

            foreach (var error in properties.SelectMany(x => x.Errors))
            {
                Errors.Add(error);
            }

            IsDirty = properties.Any(x => x.IsDirty);

            return IsValid = !properties.Any(x => !x.IsValid) && !Errors.Any();
        }

        private IEnumerable<PropertyInfo> _properties;
        protected IEnumerable<PropertyInfo> Properties()
        {
            if (_properties != null)
            {
                return _properties;
            }

            var typeinfo = typeof(IValidatableProperty).GetTypeInfo();
            var properties = typeof(T).GetRuntimeProperties();
            return _properties = properties.Where(x => typeinfo.IsAssignableFrom(x.PropertyType.GetTypeInfo()));
        }
    }
}
